import 'package:firebase/Page_Root.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
final auth = FirebaseAuth.instance;

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ChatApp',
      home: Scaffold(
        appBar: AppBar(
          title: Text('ChatApp'),
          actions :<Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: (){
              _signOut();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => new RootPage(),
                ),
              );
            },
          ),
      ]
        ),
        body: Center(
          child: Text('Hello World'),
        ),
      ),
    );
  }

  //******* Logout Function *******\\
  Future _signOut() async {
    await auth.signOut();
    Fluttertoast.showToast(msg: "Successfully Logout ",
        backgroundColor: Colors.transparent,
        textColor: Colors.green);
  }

}